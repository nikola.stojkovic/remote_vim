FROM archlinux

# RUN pacman -Suy

RUN pacman -Sy

RUN pacman -S --noconfirm git zsh wget curl tmux bash vim perl coreutils shadow bash python python-pip neovim

RUN echo 'root:root' | chpasswd

RUN useradd --create-home --shell /bin/zsh nikola

USER nikola
WORKDIR /home/nikola

RUN sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

RUN mkdir -p /home/nikola/.config/tmux/

RUN git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

COPY --chown=nikola:nikola tmux.conf /home/nikola/.config/tmux/tmux.conf

RUN sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

RUN mkdir -p /home/nikola/.config/nvim/

COPY --chown=nikola:nikola init.vim /home/nikola/.config/nvim/init.vim

ENV PATH="/home/nikola/.local/bin:${PATH}"

ENV TERM="xterm-256color"

RUN pip install 'python-language-server[all]'

RUN nvim +PlugInstall +qall

RUN ~/.tmux/plugins/tpm/scripts/install_plugins.sh

RUN git clone https://github.com/Python-World/python-mini-projects

ENTRYPOINT ["tmux"]
# ENTRYPOINT ["zsh"]
